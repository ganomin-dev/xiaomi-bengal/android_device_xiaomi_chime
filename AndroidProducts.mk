#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/statix_chime.mk

COMMON_LUNCH_CHOICES := \
    statix_chime-user \
    statix_chime-userdebug \
    statix_chime-eng
